import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable()
export class UserService {

    public user: User;
    users: User[];

    constructor() {
      this.users = [];
    }

    getUsers(): User[] {
      if(localStorage.getItem('users') === null){
        this.users = [];
      }else {
        this.users = JSON.parse(localStorage.getItem('users'));
      }
      return this.users;
    }

    addUser(user: User):void {
      this.users.unshift(user);
      let users;
      if(localStorage.getItem('users') === null){
        users = [];
        users.unshift(user);
        localStorage.setItem('users', JSON.stringify(users));
      }else{
        users = JSON.parse(localStorage.getItem('users'));
        users.unshift(user);
        localStorage.setItem('users', JSON.stringify(users));
      }
    }

    updateUser(user: User):void {
      let users;
      for (let i = 0; i < this.users.length; i++) {
        if (user.username == this.users[i].username) {
          this.users.splice(i, 1);
          users = this.users;
          users.unshift(user);
          localStorage.setItem('users', JSON.stringify(users));
        }
      }
    }

    removeUser(index) {
      let users;
      this.users.splice(index, 1);
      users = this.users;
      localStorage.setItem('users', JSON.stringify(users));
    }

}
