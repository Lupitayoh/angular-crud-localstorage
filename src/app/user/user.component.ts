import { Component, OnInit,  Output, EventEmitter } from '@angular/core';
import {User} from '../models/user';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  username: string;
  email: string;
  password: string;

  update = false;

  users: User[];
  user: User;

  constructor(
    public userService: UserService
  ) {
    this.user = new User();
  }

  ngOnInit() {
      this.users = this.userService.getUsers();
  }


  onSubmit() {
    if(this.update){
      this.userService.updateUser(this.user);
      this.update = false;
    }else{
      this.userService.addUser(this.user);
    }
    this.user = new User();

  }

  editUser(user: User) {
    this.user = user;
    this.update = true;
  }

  deleteUser(index) {
    if(confirm('Are you sure you want to delete this User?')) {
      this.userService.removeUser(index);
    }
  }
}
